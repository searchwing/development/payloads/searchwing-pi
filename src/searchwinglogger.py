import logging
from datetime import datetime
import os
import sys


######################
# SearchWing logging
######################
def getLogger(name=None, log_level="ERROR", log_path="/tmp"):
    logger = logging.getLogger()
    logger.setLevel(log_level)

    fmt = "[%(asctime)s]-[%(levelname)s]:\t%(message)s"
    formatter = MillisecondsFormatter(fmt)

    # log to stdout
    consoleHandler = logging.StreamHandler(sys.stdout)
    consoleHandler.setFormatter(formatter)
    logger.addHandler(consoleHandler)

    # log to file
    log_path = os.path.join(log_path, name + ".log")
    fh = logging.FileHandler(log_path)
    fh.setFormatter(formatter)
    logger.addHandler(fh)

    logger.info("Logfile path: {}".format(log_path))

    return logger


class MillisecondsFormatter(logging.Formatter):
    converter = datetime.fromtimestamp

    def formatTime(self, record, datefmt=None):
        ct = self.converter(record.created)
        if datefmt:
            s = ct.strftime(datefmt)
        else:
            t = ct.strftime("%Y-%m-%d %H:%M:%S")
            s = "%s.%03d" % (t, record.msecs)
        return s
