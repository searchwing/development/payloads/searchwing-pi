import time
from .modules.SelfTestResult import *
from pymavlink import mavutil
from pymavlink.dialects.v20 import ardupilotmega as mavlink
from pymavlink.dialects.v20 import ardupilotmega as mavlink2


class SelfTestMavlinkSender():
    """

    """

    def __init__(self, mavlinkEndpoint, source_system=1, source_component=10, buzzer_target_system=1, buzzer_target_component=1):
        self.buzzer_target_system = buzzer_target_system
        self.buzzer_target_component = buzzer_target_component
        self.master = mavutil.mavlink_connection(
            mavlinkEndpoint, source_system=source_system, source_component=source_component)

    def send_result(self, results):
        infoResults = []
        warningResults = []
        errorResults = []
        for oneResult in results:
            if oneResult["Result"]["Status"] == TEST_STATUS_NOTRUN or oneResult["Result"]["Status"] == TEST_STATUS_ERROR:
                errorResults.append(oneResult)
            if oneResult["Result"]["Status"] == TEST_STATUS_WARNING:
                warningResults.append(oneResult)
            if oneResult["Result"]["Status"] == TEST_STATUS_OK:
                infoResults.append(oneResult)

        # audio user feedback
        try:
            if len(errorResults) > 0:
                tune = b'CCCC'  # b'ACAB'
            else:
                tune = b'CEGH'
            tune_msg = mavlink2.MAVLink_play_tune_message(
                self.buzzer_target_system, self.buzzer_target_component, tune, b'')
            self.master.mav.send(tune_msg)
        except:
            print("Error while trying to send PLAY_TUNE Mavlink message.")

        # qgc user feedback
        hostname = open('/etc/hostname').read()
        string = hostname.rstrip(
            "\n") + ": Preflight selftest of the payload..."
        self.master.mav.statustext_send(mavutil.mavlink.MAV_SEVERITY_INFO, str.encode(
            string))  # is sent as broadcast message

        for oneErrorResult in errorResults:
            string = oneErrorResult["SelfTestModule"] + \
                " " + oneErrorResult["Result"]["Reason"]
            self.master.mav.statustext_send(mavutil.mavlink.MAV_SEVERITY_ERROR, str.encode(
                string))  # is sent as broadcast message
            # time.sleep(0.5)
        for oneWarningResult in warningResults:
            string = oneWarningResult["SelfTestModule"] + \
                " " + oneWarningResult["Result"]["Reason"]
            self.master.mav.statustext_send(mavutil.mavlink.MAV_SEVERITY_WARNING, str.encode(
                string))  # is sent as broadcast message
            # time.sleep(0.5)
        for oneInfoResult in infoResults:
            string = oneInfoResult["SelfTestModule"] + \
                " " + oneInfoResult["Result"]["Reason"]
            self.master.mav.statustext_send(mavutil.mavlink.MAV_SEVERITY_INFO, str.encode(
                string))  # is sent as broadcast message
            # time.sleep(0.5)

        if len(errorResults) > 0:
            string = "some payload dont work. You shouldnt takeoff!"
            self.master.mav.statustext_send(mavutil.mavlink.MAV_SEVERITY_ERROR, str.encode(
                string))  # is sent as broadcast message
        if len(errorResults) == 0:
            string = "payload work as expected. Have a nice flight!"
            self.master.mav.statustext_send(mavutil.mavlink.MAV_SEVERITY_INFO, str.encode(
                string))  # is sent as broadcast message
