TEST_STATUS_NOTRUN = -1
TEST_STATUS_ERROR = 0
TEST_STATUS_WARNING = 1
TEST_STATUS_OK = 2

TEST_STATUS_STR_DECODE = {
    -1: ("NOTRUN"),
    0: ("ERROR"),
    1: ("WARNING"),
    2: ("OK")
}


class SelfTestResult():
    status = TEST_STATUS_NOTRUN
    reason = TEST_STATUS_STR_DECODE[status]
    duration = 0
    start_time = 0
    end_time = 0

    def __init__(self):
        pass

    def to_json(self):
        return {'Status': self.status, 'Reason': self.reason, 'Duration': self.duration, 'StartTime': self.start_time, 'EndTime': self.end_time}

    def __str__(self):
        return str(self.to_json())
