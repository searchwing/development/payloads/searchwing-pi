from .SelfTestResult import SelfTestResult
from .SelfTestResult import *


class SelfTestModuleBase():
    """
    Base class for SelfTestModules.

    Holds the name and the result, if any.

    """

    def __init__(self):
        self.name = "Base"
        self.result = SelfTestResult()

    def run(self):
        raise NotImplementedError("Please implement the run method!")

    def set_result(self, status, reason=""):
        self.result.status = status
        self.result.reason = reason

    def print_result(self):
        print("[{}] {}: {}".format(TEST_STATUS_STR_DECODE[self.result.status],
                self.name,self.result.reason))
