from .SelfTestModuleBase import SelfTestModuleBase
from .SelfTestResult import *
import shutil


class SelfTestModuleValueInFile(SelfTestModuleBase):
    def __init__(self, name, path, direction, warningTresh, errorTresh):
        super().__init__()
        self.name = name
        self.path = path
        self.warningTresh = warningTresh
        self.errorTresh = errorTresh
        self.direction = direction

    def run(self):
        try:
            with open(self.path, 'r') as file:
                valueStr = file.read()
                value = float(valueStr)
                file.close()
                valueStrCropped = "{:.1f}".format(value)
            if self.direction == "<":
                if value < self.warningTresh:
                    self.set_result(
                        TEST_STATUS_OK, "Val:"+valueStrCropped+"<Warn:"+str(self.warningTresh))
                elif value < self.errorTresh:
                    self.set_result(TEST_STATUS_WARNING, "Val:" +
                                    valueStrCropped+"<Err:"+str(self.errorTresh))
                elif value >= self.errorTresh:
                    self.set_result(TEST_STATUS_ERROR, "Val:" +
                                    valueStrCropped+">Err:"+str(self.errorTresh))
            elif self.direction == ">":
                if value > self.warningTresh:
                    self.set_result(
                        TEST_STATUS_OK, "Val:"+valueStrCropped+">Warn:"+str(self.warningTresh))
                elif value > self.errorTresh:
                    self.set_result(TEST_STATUS_WARNING, "Val:" +
                                    valueStrCropped+">Err:"+str(self.errorTresh))
                elif value <= self.errorTresh:
                    self.set_result(TEST_STATUS_ERROR, "Val:" +
                                    valueStrCropped+"<Err:"+str(self.errorTresh))
            else:
                self.set_result(TEST_STATUS_ERROR,
                                "Unsupported direction to compare value to")
        except:
            self.set_result(TEST_STATUS_ERROR, "unknown error")
