from .SelfTestModuleBase import SelfTestModuleBase
from .SelfTestResult import *
import datetime
import pathlib
import time


class SelfTestModuleCameraImages(SelfTestModuleBase):
    def __init__(self, name, imageFile, timeout):
        super().__init__()
        self.name = name
        self.imageFile = imageFile
        self.timeout = timeout

    def run(self):
        try:
            start_stamp = datetime.datetime.now()
            time.sleep(self.timeout)
            image_FileName = pathlib.Path(self.imageFile)
            image_stamp = datetime.datetime.fromtimestamp(
                image_FileName.stat().st_mtime)
            max_stamp = start_stamp + datetime.timedelta(seconds=self.timeout)
            # check if new image is recorded since start_stamp + timeout
            if image_stamp > start_stamp and image_stamp < max_stamp:
                self.set_result(
                    TEST_STATUS_OK, "preflight image saved in time!")
            else:
                self.set_result(TEST_STATUS_ERROR, "can't take/save photos!")
        except:
            self.set_result(TEST_STATUS_ERROR, "unknown error")
