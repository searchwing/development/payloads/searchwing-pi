from .SelfTestModuleBase import SelfTestModuleBase
from .SelfTestResult import *
from src.zmqDrivers.zmqConnection import zmqReceiver


class SelfTestModuleValueZmqReceiver(SelfTestModuleBase):
    def __init__(self, name, topic, direction, warningTresh, errorTresh):
        super().__init__()
        self.name = name
        self.topic = topic
        self.receiver = zmqReceiver(topic)
        self.receiver.set_receive_timeout(100)
        self.warningTresh = warningTresh
        self.errorTresh = errorTresh
        self.direction = direction

    def run(self):
        try:
            msg = self.receiver.receive_msg()
            if msg is None:
                self.set_result(TEST_STATUS_ERROR,
                                "No data from " + self.topic)
                return
            value = msg['data']
            valueStrCropped = "{:.1f}".format(value)
            if self.direction == "<":
                if value <= self.warningTresh:
                    self.set_result(
                        TEST_STATUS_OK, "Val:"+valueStrCropped+"<Warn:"+str(self.warningTresh))
                elif value < self.errorTresh:
                    self.set_result(TEST_STATUS_WARNING, "Val:" +
                                    valueStrCropped+"<Err:"+str(self.errorTresh))
                elif value >= self.errorTresh:
                    self.set_result(TEST_STATUS_ERROR, "Val:" +
                                    valueStrCropped+">Err:"+str(self.errorTresh))
            elif self.direction == ">":
                if value > self.warningTresh:
                    self.set_result(
                        TEST_STATUS_OK, "Val:"+valueStrCropped+">Warn:"+str(self.warningTresh))
                elif value > self.errorTresh:
                    self.set_result(TEST_STATUS_WARNING, "Val:" +
                                    valueStrCropped+">Err:"+str(self.errorTresh))
                elif value <= self.errorTresh:
                    self.set_result(TEST_STATUS_ERROR, "Val:" +
                                    valueStrCropped+"<Err:"+str(self.errorTresh))
            else:
                self.set_result(TEST_STATUS_ERROR,
                                "Unsupported direction to compare value to")
        except:
            self.set_result(TEST_STATUS_ERROR, "unknown error")
