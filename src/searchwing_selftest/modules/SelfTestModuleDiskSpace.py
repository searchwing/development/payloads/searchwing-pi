from .SelfTestModuleBase import SelfTestModuleBase
from .SelfTestResult import *
import shutil


class SelfTestModuleDiskSpace(SelfTestModuleBase):
    def __init__(self, path, minSpaceAbs):
        super().__init__()
        self.name = "DiskSpace"
        self.path = path
        self.minSpaceAbs = minSpaceAbs

    def run(self):
        try:
            total, used, free = shutil.disk_usage(self.path)
            free = free//(2**30)
            total = total//(2**30)
            if free >= self.minSpaceAbs:
                self.set_result(TEST_STATUS_OK, str(
                    self.minSpaceAbs) + "GB Needed - Free " + str(free) + "/" + str(total) + "GB")
            else:
                self.set_result(TEST_STATUS_ERROR, str(
                    self.minSpaceAbs) + "GB Needed - Free " + str(free) + "/" + str(total) + "GB")
        except:
            self.set_result(TEST_STATUS_ERROR, "unknown error")
