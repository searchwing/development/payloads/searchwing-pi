
class SelfTest():
    """

    """

    def __init__(self, modules):
        self.result = None
        self._modules = modules

    def run_all(self):
        for module in self._modules:
            module.run()

    def run(self, module_name):
        pass

    def print_result(self):
        for module in self._modules:
            module.print_result()

    def save_result(self, file_path):
        pass

    def get_results(self):
        results = []
        for module in self._modules:
            results.append({"SelfTestModule": module.name,
                           "Result": module.result.to_json()})
        return results
