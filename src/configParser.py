import os
import sys
import toml


class ConfigParser():
    def __init__(self, path=""):
        # if no path is given use a default path
        if path == "":
            path = os.path.join(
                os.path.dirname(__file__),
                "..",
                "configuration.toml"
            )

        self.config = self._parse(path)

    def _parse(self, path):
        with open(path, 'r') as f:
            return toml.loads(f.read())

    def getConfig(self):
        return self.config

    def printConfig(self, logger):
        for key, value in self.config.items():
            logger.info("{}: {} ".format(key, value))
