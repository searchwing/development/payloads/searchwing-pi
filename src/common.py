from pymavlink.dialects.v10 import common as commonMavlinkDialect

MAV_STATE_DECODE = {
    0: ("MAV_STATE_UNINIT"),         # Uninitialized system, state is unknown.
    1: ("MAV_STATE_BOOT"),           # System is booting up.
    # System is calibrating and not flight-ready.
    2: ("MAV_STATE_CALIBRATING"),
    # System is grounded and on standby. It can be launched any time.
    3: ("MAV_STATE_STANDBY"),
    # System is active and might be already airborne. Motors are engaged.
    4: ("MAV_STATE_ACTIVE"),
    # System is in a non-normal flight mode. It can however still navigate.
    5: ("MAV_STATE_CRITICAL"),
    # System is in a non-normal flight mode. It lost control over parts of the plane.
    6: ("MAV_STATE_EMERGENCY"),
    # System just initialized its power-down sequence, will shut down now.
    7: ("MAV_STATE_POWEROFF"),
}


def checkMavInFlight(heartbeatMsg):
    mavInFlight = bool(heartbeatMsg.base_mode &
                       commonMavlinkDialect.MAV_MODE_FLAG_SAFETY_ARMED)
    return mavInFlight
