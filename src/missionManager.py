import os
from datetime import datetime
from searchwinglogger import getLogger


class MissionManager():
    def __init__(self, state, config, logger):
        self.state = state
        self.config = config
        self.logger = logger

        self.units = []
        for oneUnit in self.config['UNIT']:
            self.units.append(oneUnit)

        # Create folders for dummy "nomission"
        self.noMissionDir = os.path.join(
            self.config['CAM_IMG_STORAGE_DIR'], "noMission")
        for oneUnit in self.units:
            noMissionUnitDir = os.path.join(
                self.noMissionDir, self.config['UNIT'][oneUnit]['FOLDER_NAME'])
            if not os.path.exists(noMissionUnitDir):
                os.makedirs(noMissionUnitDir)

        self.currentMissionDir = self.noMissionDir
        self.droneName = open('/etc/hostname').read()

    def processState(self, newState):

        # EVENT: Arming / Takeoff
        if newState["ISFLYING"] and (not self.state["ISFLYING"] or self.state["ISFLYING"] is None):
            self.logger.info(
                "[MISSION_MANAGER] EVENT: Arming / Takeoff detected!")
            missionStartStamp = datetime.now()
            missionStartStampString = missionStartStamp.strftime(
                "%Y-%m-%dT%H-%M-%S")
            self.currentMissionDir = os.path.join(
                self.config['CAM_IMG_STORAGE_DIR'], self.droneName+"_"+missionStartStampString)
            self.logger.info(
                "[MISSION_MANAGER] Create folders for new mission: "+self.currentMissionDir)

            # Create folders for each unit for new mission
            for oneUnit in self.units:
                missionUnitDir = os.path.join(
                    self.currentMissionDir, self.config['UNIT'][oneUnit]['FOLDER_NAME'])
                if not os.path.exists(missionUnitDir):
                    os.makedirs(missionUnitDir)

        # EVENT: Disarm / Landing
        elif not newState["ISFLYING"] and (self.state["ISFLYING"] or self.state["ISFLYING"] is None):
            self.logger.info(
                "[MISSION_MANAGER] EVENT: Disarm / Landing detected!")
            self.currentMissionDir = self.noMissionDir

        self.state = newState
        return self.currentMissionDir
