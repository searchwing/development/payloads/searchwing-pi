#!/usr/bin/env python3

import sys
import os
sys.path.insert(0, os.path.join(
    os.path.dirname(os.path.abspath(__file__)), '..'))
import Adafruit_DHT
from configParser import ConfigParser
import time
from zmqConnection import zmqSender


class Dht22Driver():

    def __init__(self):
        self.tempSender = zmqSender("ipc:///dev/shm/dht22_temp")
        self.humidSender = zmqSender("ipc:///dev/shm/dht22_hum")

        configParser = ConfigParser()
        config = configParser.getConfig()
        self.DHT_PIN = config["DHT_PIN"]
        self.DHT_SENSOR = Adafruit_DHT.DHT22

    def run(self):
        while True:
            hum, temp = Adafruit_DHT.read(self.DHT_SENSOR, self.DHT_PIN)
            if(hum is not None and temp is not None):
                self.tempSender.send_msg(time.time(), temp)
                self.humidSender.send_msg(time.time(), hum)
            time.sleep(1)


if __name__ == '__main__':
    d = Dht22Driver()
    d.run()
