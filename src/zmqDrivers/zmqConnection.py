import zmq
import pickle
import traceback


class zmqSender():
    def __init__(self, topic):
        assert topic != "", "Set topic name"

        context = zmq.Context()
        self.socket = context.socket(zmq.PUB)
        # only keep latest message in buffer
        self.socket.setsockopt(zmq.CONFLATE, 1)
        self.socket.bind(topic)

    def send_msg(self, timestamp: float, data):
        msg = {"timestamp": timestamp, "data": data}
        self.socket.send(pickle.dumps(msg))


class zmqReceiver():
    def __init__(self, topic, logger=None):
        assert topic != "", "Set topic name"

        context = zmq.Context()
        self.socket = context.socket(zmq.SUB)
        # only keep latest message in buffer
        self.socket.setsockopt(zmq.CONFLATE, 1)
        self.socket.connect(topic)
        self.socket.setsockopt_string(zmq.SUBSCRIBE, "")

        self.logger = logger
        self.topic = topic

    def set_receive_timeout(self, timeout):
        if timeout > 0:
            self.socket.setsockopt(zmq.RCVTIMEO, timeout)

    def receive_msg(self):
        try:
            pickled = self.socket.recv()
            msg = pickle.loads(pickled)
            return msg
        except Exception as e:
            if not self.logger is None:
                self.logger.error("[ZMQ] receive " + self.topic + " " + str(e))
            return None
