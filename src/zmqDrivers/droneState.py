#!/usr/bin/env python3

import sys
import os
sys.path.insert(0, os.path.join(
    os.path.dirname(os.path.abspath(__file__)), '..'))
from missionManager import MissionManager
from externalExifWriting import write_exif_metadata, rad_to_deg
from searchwinglogger import getLogger
from common import *
from configParser import ConfigParser
from zmqConnection import zmqSender
from pymavlink import mavutil
import time
from datetime import datetime
from pymavlink.dialects.v10 import common as commonMavlinkDialect

msg_types = [
    "SYSTEM_TIME",
    "GLOBAL_POSITION_INT",
    "LOCAL_POSITION_NED",
    "ATTITUDE",
    "HEARTBEAT"
]


class DroneStateDriver():

    def __init__(self):

        configParser = ConfigParser()
        self.config = configParser.getConfig()
        self.logger = getLogger("searchwing-zmq-DroneState",
                                self.config['LOGGER_LEVEL'], self.config['LOG_STORAGE_DIR'])

        self.state = {}
        self.state["GLOBAL_POSITION_INT"] = None
        self.state["LOCAL_POSITION_NED"] = None
        self.state["ATTITUDE"] = None
        self.state["ISFLYING"] = None
        self.state["MISSIONDIR"] = None

        self.stateSender = zmqSender("ipc:///dev/shm/droneState")
        self.master = mavutil.mavlink_connection(
            self.config['MAVLINK_ENDPOINT'])

        self.linux_time_set = False

        self.missionManger = MissionManager(
            self.state.copy(), self.config, self.logger)  # copy => do call by value

    def handle_msg(self, msg):

        msg_type = msg.get_type()
        state_update = False

        if msg_type == "SYSTEM_TIME":
            if not self.linux_time_set:
                # Timestamp based on GPS
                # get RTC time from ardupilot which is UTC if following is set to GPS:
                # http://ardupilot.org/copter/docs/parameters.html#brd-rtc-types-allowed-sources-of-rtc-time
                # The utc time can be aquired by reading the RTC based SYSTEM_TIME via MAVLINK:
                # https://github.com/ArduPilot/ardupilot/blob/378d5c7a5abd7ed2d96f00bcc63f0dbabbc523e9/libraries/GCS_MAVLink/GCS_Common.cpp#L1488
                now_pixracer = datetime.fromtimestamp(
                    msg.time_unix_usec / 1000000)
                if self.config['LOGGER_LEVEL'] == "DEBUG":
                    self.logger.debug("[SYSTEM_TIME] Pixracer time: {}".format(
                        now_pixracer.isoformat()))
                if now_pixracer.year >= 2021:
                    setdatecmd = "date --set '{}'".format(
                        now_pixracer.isoformat())
                    returned_value = os.system(setdatecmd)
                    self.logger.info("[SYSTEM_TIME] Companion time set to Pixracer time: {}".format(
                        now_pixracer.isoformat()))

                    self.linux_time_set = True
                else:
                    self.logger.debug(
                        "[SYSTEM_TIME] Discard Pixracer timestamp as it is not valid (year < 2021!)")
        elif msg_type == "GLOBAL_POSITION_INT":
            self.state["GLOBAL_POSITION_INT"] = msg
            if self.config['LOGGER_LEVEL'] == "DEBUG":
                self.logger.debug("[MAVLINK GPS msg]: (lat: {}, lon: {}, hdg: {}°)".format(
                    msg.lat / 10e6,
                    msg.lon / 10e6,
                    msg.hdg / 10e2
                ))
            state_update = True
        elif msg_type == "LOCAL_POSITION_NED":
            self.state["LOCAL_POSITION_NED"] = msg
            if self.config['LOGGER_LEVEL'] == "DEBUG":
                self.logger.debug("[MAVLINK local_pos msg]: (x: {0:8.2f}, y: {1:8.2f}, z: {2:8.2f})".format(
                    msg.x,
                    msg.y,
                    msg.z
                ))
            state_update = True
        elif msg_type == "ATTITUDE":
            self.state["ATTITUDE"] = msg
            if self.config['LOGGER_LEVEL'] == "DEBUG":
                self.logger.debug("[MAVLINK attitude msg]: (roll: {0:8.2f}°, pitch: {1:8.2f}°, yaw: {2:8.2f}°)".format(
                    rad_to_deg(msg.roll),
                    rad_to_deg(msg.pitch),
                    rad_to_deg(msg.yaw)
                ))
            state_update = True
        elif msg_type == "HEARTBEAT":
            if msg.type == commonMavlinkDialect.MAV_TYPE_FIXED_WING:
                isFlying = checkMavInFlight(msg)
                self.state["ISFLYING"] = isFlying

                if self.config['LOGGER_LEVEL'] == "DEBUG":
                    self.logger.debug("[HEARTBEAT msg]: Mode: {}, State: {}".format(
                        msg.base_mode, MAV_STATE_DECODE[msg.system_status]))

                missionDir = self.missionManger.processState(self.state.copy())
                self.state["MISSIONDIR"] = missionDir

                state_update = True
        else:
            if self.config['LOGGER_LEVEL'] == "DEBUG":
                self.logger.debug(
                    "[MAVLINK]: unknown or not supported message")

        return state_update

    def run(self):
        while True:
            msg = self.master.recv_match(type=msg_types, blocking=True)
            stamp = time.time()
            state_update = self.handle_msg(msg)
            if state_update:
                self.stateSender.send_msg(stamp, self.state)


if __name__ == '__main__':
    d = DroneStateDriver()
    d.run()
