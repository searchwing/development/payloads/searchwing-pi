#!/usr/bin/env python3

import sys
import os
from zmqConnection import zmqSender
import time


class CpuTempDriver():

    def __init__(self):
        self.tempSender = zmqSender("ipc:///dev/shm/cputemp")

    def run(self):
        while True:
            # Get current temperature
            temp = int(
                open('/sys/class/thermal/thermal_zone0/temp').read()) / 1e3
            stamp = time.time()
            self.tempSender.send_msg(stamp, temp)

            time.sleep(1)


if __name__ == '__main__':
    d = CpuTempDriver()
    d.run()
