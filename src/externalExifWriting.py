import math
from fractions import Fraction
import piexif
import piexif.helper


def rad_to_deg(rad):
    return rad*180.0/math.pi


# from https://gist.github.com/NeoFarz/27f35ec2f84f5c52394cea3891c18832
def to_deg(value, loc):
    # convert decimal coordinates into degrees, minutes and seconds tuple
    # Keyword arguments:
    #   value is float gps-value,
    #   loc is direction list ["S", "N"] or ["W", "E"]
    # return: tuple like (25, 13, 48.343 ,'N')
    if value < 0:
        loc_value = loc[0]
    elif value > 0:
        loc_value = loc[1]
    else:
        loc_value = ""
    abs_value = abs(value)
    deg = int(abs_value)
    t1 = (abs_value - deg) * 60
    min = int(t1)
    sec = round((t1 - min) * 60, 5)
    return (deg, min, sec, loc_value)


def change_to_rational(number):
    # convert a number to rantional
    # Keyword arguments: number
    # return: tuple like (1, 2), (numerator, denominator)
    f = Fraction(str(number))
    return (f.numerator, f.denominator)


# Slow method for writing exif data when using simulation. Adds GPS position +
# local pose as EXIF metadata.
def write_exif_metadata(file_path: str, lat: float, lng: float, altitude: float, heading: float, localPos, localOrient, stamp):
    # TODO: change existing exif data instead of overwriting it

    lat_deg = to_deg(lat, ["S", "N"])
    lng_deg = to_deg(lng, ["W", "E"])

    exiv_lat = (change_to_rational(lat_deg[0]), change_to_rational(
        lat_deg[1]), change_to_rational(lat_deg[2]))
    exiv_lng = (change_to_rational(lng_deg[0]), change_to_rational(
        lng_deg[1]), change_to_rational(lng_deg[2]))

    gps_ifd = {
        piexif.GPSIFD.GPSVersionID: (2, 0, 0, 0),
        piexif.GPSIFD.GPSAltitudeRef: 0,
        piexif.GPSIFD.GPSAltitude: change_to_rational(round(altitude)),
        piexif.GPSIFD.GPSLatitudeRef: lat_deg[3],
        piexif.GPSIFD.GPSLatitude: exiv_lat,
        piexif.GPSIFD.GPSLongitudeRef: lng_deg[3],
        piexif.GPSIFD.GPSLongitude: exiv_lng,
        piexif.GPSIFD.GPSImgDirectionRef: "T",  # T=True North, M=Magnetic North
        piexif.GPSIFD.GPSImgDirection: change_to_rational(
            float('%.2f' % (heading)))  # only save with 2 decimal precessions
    }
    exif_ifd = {
        piexif.ExifIFD.DateTimeOriginal: stamp.strftime("%Y:%m:%d %H:%M:%S"),
        piexif.ExifIFD.SubSecTimeOriginal: stamp.strftime("%f")
    }
    exif_dict = {"Exif": exif_ifd, "GPS": gps_ifd}

    # add pose as csv using Exif-UserComment field
    if localPos is not None and localOrient is not None:
        localpose_as_csv = "LocalPosX,LocalPosY,LocalPosZ,OrientX,OrientY,OrientZ\n{},{},{},{},{},{}"
        localpose_as_csv = localpose_as_csv.format(
            localPos.x,
            localPos.y,
            localPos.z,
            localOrient.roll,
            localOrient.pitch,
            localOrient.yaw
        )
        user_comment = piexif.helper.UserComment.dump(
            localpose_as_csv, encoding="ascii")
        exif_dict["Exif"][piexif.ExifIFD.UserComment] = user_comment

    exif_bytes = piexif.dump(exif_dict)
    piexif.insert(exif_bytes, file_path)
