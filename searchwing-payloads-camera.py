#!/usr/bin/env python3

import time
from datetime import datetime
import os
import math
import time
import sys
import shutil
import picamera
from src.common import *
from src.searchwinglogger import getLogger
from src.configParser import ConfigParser
from src.externalExifWriting import write_exif_metadata, rad_to_deg
from src.zmqDrivers.zmqConnection import zmqReceiver


class searchwing_payload_camera():

    def __init__(self, unit_name):
        configParser = ConfigParser()
        self.config = configParser.getConfig()
        self.config_unit = self.config['UNIT'][unit_name]
        self.unit_name = unit_name

        self.logger = getLogger("searchwing-payloads-camera-"+unit_name,
                                self.config['LOGGER_LEVEL'], self.config['LOG_STORAGE_DIR'])
        configParser.printConfig(self.logger)

        self.logger.info("init cam...")
        self.camera = picamera.PiCamera(self.config_unit['PI_CAM_NUMBER'])
        self.camera.resolution = self.config['CAM_RESOLUTION']
        self.camera.exposure_mode = self.config['CAM_EXPOSURE_MODE']
        self.exposureLoopSettings = self.config['CAM_EXPOSURE_LOOP']
        if self.config_unit['CAM_ROTATION'] == "forward":
            self.camera.vflip = False
            self.camera.hflip = False
        if self.config_unit['CAM_ROTATION'] == "backward":
            self.camera.vflip = True
            self.camera.hflip = True
        self.imageUniqueID = 0
        
        self.logger.info("init folders...")
        if not os.path.exists(self.config['CAM_IMG_STORAGE_DIR']):
            self.logger.error("Image save path doesnt exist : {} , ABORT! ".format(
                self.config['CAM_IMG_STORAGE_DIR']))
            sys.exit("")

        self.latestImgPath = os.path.join(
            self.config['CAM_IMG_STORAGE_DIR'], "0_latest_"+self.config_unit['FOLDER_NAME']+".jpg")
        self.preflightImgPath = os.path.join(
            self.config['CAM_IMG_STORAGE_DIR'], "preflight_"+self.config_unit['FOLDER_NAME']+".jpg")
        self.noMissionDir = os.path.join(
            self.config['CAM_IMG_STORAGE_DIR'], "noMission", self.config_unit['FOLDER_NAME'])

        self.droneState_Receiver = zmqReceiver(
            "ipc:///dev/shm/droneState", self.logger)
        # if not set, waits forever for new message
        self.droneState_Receiver.set_receive_timeout(50)
        self.lastDroneStateMsg = None  # save last message


    def camera_loop(self):

        while True:
            droneStateMsg = self.droneState_Receiver.receive_msg()

            # check if valid "ISFLYING" msg is received and if not assume flying
            if droneStateMsg is None:
                droneStateValid = False
                droneStateInFlight = True
                droneStateMsg = self.lastDroneStateMsg  # use last avaliable state
            else:
                droneStateValid = True
                droneStateInFlight = droneStateMsg["data"]["ISFLYING"]

            localPosition = None
            globalPosition = None
            attitude = None
            if not droneStateMsg is None:
                self.logger.debug("[STATE] stamp: " +
                                  str(droneStateMsg["timestamp"]))
                localPosition = droneStateMsg["data"]["LOCAL_POSITION_NED"]
                globalPosition = droneStateMsg["data"]["GLOBAL_POSITION_INT"]
                attitude = droneStateMsg["data"]["ATTITUDE"]
                self.lastDroneStateMsg = droneStateMsg

            # Should we enable the camera?
            enableCamera = \
                (droneStateValid and droneStateInFlight) or \
                (self.config['CAM_ONLY_INFLIGHT'] == False) or \
                (self.config['CAM_ONLY_INFLIGHT']
                 == True and not droneStateValid)

            # Generate filename
            picStamp = datetime.now()
            filename = None
            if not enableCamera:
                filepath = self.preflightImgPath
            else:  # in flight
                filename = picStamp.strftime("%Y-%m-%dT%H-%M-%S.%f") + ".jpg"
                # no or invalid message received from dronestate
                if droneStateMsg is None or droneStateMsg["data"]["MISSIONDIR"] is None:
                    # save to dummy "noMission" folder just in case
                    filepath = os.path.join(self.noMissionDir, filename)
                else:
                    filepath = os.path.join(
                        droneStateMsg["data"]["MISSIONDIR"], self.config_unit['FOLDER_NAME'], filename)

            # Take actual photo
            start = time.time()
            self.take_photo(image_path=filepath, gps_pos=globalPosition,
                            local_pos=localPosition, local_orient=attitude, datetime=picStamp)
            last_pic_timestamp = datetime.timestamp(picStamp)
            end = time.time()
            self.logger.debug(
                "[PHOTO] image full capture took [s]:{0:8.3f}".format(end - start))

            # sleep until next photo
            cam_next_image_duration = 0
            if enableCamera:
                cam_next_image_duration = self.config['CAM_TRIGGER_PERIODE']
            else:
                cam_next_image_duration = self.config['CAM_TRIGGER_PERIODE_PREFLIGHT']
            now_timestamp = datetime.timestamp(datetime.now())
            dt = now_timestamp-last_pic_timestamp
            if dt > cam_next_image_duration or dt < 0:  # prevent strange system time changes via mavlink
                dt = cam_next_image_duration
            time.sleep(cam_next_image_duration-dt)

    # takes a jpg photo with given name and gps position
    def take_photo(self, image_path, datetime, gps_pos=None, local_pos=None, local_orient=None):
        if gps_pos:
            self.set_picam_exif_metadata(gps_pos.lat / 10e6, gps_pos.lon / 10e6,
                                         gps_pos.alt / 1e3, gps_pos.hdg / 10e1, local_pos, local_orient, datetime)

        exposureSettingCurrentIdx = self.imageUniqueID % len(self.exposureLoopSettings)
        exposureSettingCurrentIdxNext = (self.imageUniqueID+1) % len(self.exposureLoopSettings)
        exposureSetting = self.exposureLoopSettings[exposureSettingCurrentIdx]
        exposureSettingNext = self.exposureLoopSettings[exposureSettingCurrentIdxNext]

        if exposureSetting > 0:
            self.camera.shutter_speed = int( self.camera.exposure_speed * exposureSetting)
            self.camera.exposure_mode = 'off'
        else:
            self.camera.shutter_speed = 0  # set auto

        self.logger.debug("camera.analog_gain: "+ str(float(self.camera.analog_gain)))
        self.logger.debug("camera.exposure_speed: "+ str(float(self.camera.exposure_speed)))            
        self.logger.debug("camera.shutter_speed: "+ str(float(self.camera.shutter_speed)))     

        start = time.time()
        self.camera.capture(image_path, format='jpeg',
                            quality=self.config['CAM_IMG_JPEG_QUALITY'], thumbnail=None, use_video_port=False)
        end = time.time()
        duration = end-start

        if exposureSetting != 0.0 and exposureSettingNext == 0.0:
            self.camera.shutter_speed = 0  # set auto
            self.camera.exposure_mode = self.config['CAM_EXPOSURE_MODE']
            # run a inital photo for automode without saving to disk to precalibrate the gains
            self.camera.capture("/dev/shm/dummy", format='jpeg',
                                quality=self.config['CAM_IMG_JPEG_QUALITY'], thumbnail=None, use_video_port=False)

        self.logger.debug(
            "[PHOTO] image capture took [s]:{0:8.3f}".format(duration))

        if gps_pos:
            self.logger.info("[PHOTO] {} captured (lat:{}, lon:{})".format(
                image_path, gps_pos.lat / 10e6, gps_pos.lon / 10e6))
        else:
            self.logger.info("[PHOTO] {} captured ".format(image_path))

        # change owner of image to searchwing user
        shutil.chown(
            image_path, user=self.config['CAM_IMG_FILE_OWNER_GROUP'], group=self.config['CAM_IMG_FILE_OWNER_GROUP'])

        # delete symlink to last latest if exist
        try:
            os.remove(self.latestImgPath)
        except:
            pass
        # create symbolic link to point latest image to current image
        os.symlink(image_path, self.latestImgPath)

        # create symbolic link to for preflight check image in case always photos should be taken
        if self.config['CAM_ONLY_INFLIGHT'] == False:
            try:
                os.remove(self.preflightImgPath)
            except:
                pass
            os.symlink(image_path, self.preflightImgPath)

        self.imageUniqueID = self.imageUniqueID + 1

    # Fast method for writing exif when using pi by setting the parameters before saving the image
    def set_picam_exif_metadata(self, lat: float, lon: float, altitude: float, heading: float, localPos, localOrient, stamp):
        # from : https://wiki.openstreetmap.org/wiki/User:MHohmann/RPiCam

        # Set picture date and time to GPS values.
        self.camera.exif_tags['EXIF.DateTimeOriginal'] = stamp.strftime(
            '%Y:%m:%d %H:%M:%S')
        self.camera.exif_tags['EXIF.SubSecTimeOriginal'] = stamp.strftime('%f')

        # Set altitude to GPS value.
        alt = altitude
        self.camera.exif_tags['GPS.GPSAltitudeRef'] = '0' if alt > 0 else '1'
        aalt = math.fabs(alt)
        self.camera.exif_tags['GPS.GPSAltitude'] = '%d/100' % int(100 * aalt)

        # Set GPS latitude.
        self.camera.exif_tags['GPS.GPSLatitudeRef'] = 'N' if lat > 0 else 'S'
        alat = math.fabs(lat)
        dlat = int(alat)
        mlat = int(60 * (alat - dlat))
        slat = int(6000 * (60 * (alat - dlat) - mlat))
        self.camera.exif_tags['GPS.GPSLatitude'] = '%d/1,%d/1,%d/100' % (
            dlat, mlat, slat)

        # Set GPS longitude.
        self.camera.exif_tags['GPS.GPSLongitudeRef'] = 'E' if lon > 0 else 'W'
        alon = math.fabs(lon)
        dlon = int(alon)
        mlon = int(60 * (alon - dlon))
        slon = int(6000 * (60 * (alon - dlon) - mlon))
        self.camera.exif_tags['GPS.GPSLongitude'] = '%d/1,%d/1,%d/100' % (
            dlon, mlon, slon)

        # Set direction
        # T=True North, M=Magnetic North
        self.camera.exif_tags['GPS.GPSImgDirectionRef'] = 'T'
        self.camera.exif_tags['GPS.GPSImgDirection'] = '%d/100' % int(
            100 * heading)

        if localPos is not None and localOrient is not None:
            localpose_as_csv = "{},{},{},{},{},{}".format(
                localPos.x,
                localPos.y,
                localPos.z,
                localOrient.roll,
                localOrient.pitch,
                localOrient.yaw
            )
            self.camera.exif_tags['EXIF.UserComment'] = localpose_as_csv

        # Set camera name
        self.camera.exif_tags['IFD0.Artist'] = open(
            '/etc/hostname').read()+"_"+self.unit_name

        # Counter for images
        self.camera.exif_tags['EXIF.ImageUniqueID'] = str(self.imageUniqueID)


if __name__ == '__main__':
    unit_name = os.environ['SW_UNIT']

    p = searchwing_payload_camera(unit_name)
    p.camera_loop()
