from glob import glob
from skimage.transform import rescale
from skimage.measure.entropy import shannon_entropy
from skimage import io
from skimage.morphology import disk
from skimage.filters.rank import entropy
from skimage.util import img_as_ubyte
from skimage import data
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import rcParams
rcParams.update({'figure.autolayout': True})


# Second example: texture detection.

folderpath = "lampdirect"
extension = '/*.jpg'

files = sorted(glob(folderpath+extension))

for oneFile in files:
    filename = oneFile
    print(filename)
    image = io.imread(filename, as_gray=True)
    #image = rescale(image, 0.25)
    shannon_entropy_val = shannon_entropy(image)
    print("shannon_entropy_val: "+str(shannon_entropy_val))
    fig, (ax0, ax1) = plt.subplots(ncols=2, figsize=(12, 4),
                                   sharex=True, sharey=True)

    img0 = ax0.imshow(image, cmap=plt.cm.gray)
    ax0.set_title("Image")
    ax0.axis("off")
    fig.colorbar(img0, ax=ax0)

    img1 = ax1.imshow(entropy(image, disk(5)), cmap='gray')
    ax1.set_title("Entropy " + str(shannon_entropy_val))
    ax1.axis("off")
    fig.colorbar(img1, ax=ax1)

    fig.tight_layout()
    plt.savefig(filename+".fig.jpg")
    # plt.show()
