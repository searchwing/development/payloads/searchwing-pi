#!/usr/bin/env python3

import os
import toml
import time
from src.zmqDrivers.zmqConnection import zmqReceiver
from src.searchwinglogger import getLogger
from src.configParser import ConfigParser

configParser = ConfigParser()
config = configParser.getConfig()
logger = getLogger(
    "searchwing-payloads-temp-humid",
    config['LOGGER_LEVEL'],
    config['LOG_STORAGE_DIR']
)

cpuTempZmqRec = zmqReceiver("ipc:///dev/shm/cputemp", logger)
cpuTempZmqRec.set_receive_timeout(1)

dht22TempZmqRec = zmqReceiver("ipc:///dev/shm/dht22_temp", logger)
dht22TempZmqRec.set_receive_timeout(1)

dht22HumZmqRec = zmqReceiver("ipc:///dev/shm/dht22_hum", logger)
dht22HumZmqRec.set_receive_timeout(1)

while True:

    cpuTempMsg = cpuTempZmqRec.receive_msg()
    if cpuTempMsg is not None:
        logger.info("[Pi-Core-Temp °C]: {}".format(cpuTempMsg['data']))

    dht22tempMsg = dht22TempZmqRec.receive_msg()
    if dht22tempMsg is not None:
        logger.info("[DHT-Temp °C]: {:.2f}".format(dht22tempMsg['data']))

    dht22humMsg = dht22HumZmqRec.receive_msg()
    if dht22humMsg is not None:
        logger.info("[DHT-Humidity %]: {:.2f}".format(dht22humMsg['data']))

    time.sleep(1.0)
