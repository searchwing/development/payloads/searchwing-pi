Hier finden sich alle angepassten Configs und Skripte um den Pi Zero so einzurichten, dass er selbstständig alle x Sekunden ein Bild über die angeschlossene Kamera macht und diese auf der SD-Card speichert.

## Betriebsystem

Auf dem Pi ist ein Raspbian stretch lite mit Zusatzanpassungen installiert. Das Image wurde mithilfe von https://gitlab.com/searchwing/development/searchwing-pi-gen generiert. 

## Schnittstellen

Der Pi muss mit dem TELE2 Ausgang des Autopiloten verbunden sein.

## Autostart Skripte (Systemd)
Die Service-Configs für den Pi befinden sich alle im Verzechnis `root` von diesem Repository.

### searchwing-payloads-camera.service

`/etc/systemd/system/searchwing-payloads-camera.service` startet das Skript für die Bildaufnahme (`/home/searchwing/searchwing-pi/continuos_capture_GPS.py`) und ist im oben genannten Image per default aktiviert.

Mithilfe des Skripts nimmt die Kamera alle x Sekunden ein Bild auf und nutzt Metadaten im EXIF-Format:
* Drohnenname
* GPS-Position
* GPS-Zeitstempel
* Lokale Pose im Bezug zum Startpunkt (Position [m], Ausrichtung [°])

Die Bilder werden unter `/data/bilder/` abgelegt.

Das Skript hat Einstellungsmöglichkeiten, welche in `config_continuous_capture_GPS.json` gesetzt werden können:
* Kamera-Bild Frequenz
* Kamera-Bild Auflösung
* Kamera-Belichtungsmodus
* Kamera-Bild Qualität (JPEG-Qualität)
* Photos schießen nur im Flug oder immer
* Log Pfad
* Log Level

Die Logfiles von des Skripts werden unter `/data/logs/` abgelegt. Dort kann der Input via MAVLINK nachvollzogen werden. Man kann den aktuellen Zustand / Log auch mittels `systemctl status capture_images_GPS` abfragen.

### simple_webserver.service

`/etc/systemd/system/simple_webserver.service` startet einen minimalen Python3 Webserver im Verzeichnis `/data/bilder/` und gibt den Inhalt dieses Verzeichnisses aus.  

Über den Link [http://DRONE_IP/0_latest.jpg](http://DRONE_IP/0_latest.jpg) kann das zuletzt aufgenommene Bild im Browser abgerufen werden.


## Steuern von Diensten

Abfragen des Status des Bildaufnahme-Service

	sudo systemctl status searchwing-payloads-camera.service

Stoppen

	sudo systemctl stop searchwing-payloads-camera.service
	
Starten

	sudo systemctl start searchwing-payloads-camera.service
	
Beim booten automatisch starten

	sudo systemctl enable searchwing-payloads-camera.service
	
Automatisches starten deaktivieren

	sudo systemctl disable searchwing-payloads-camera.service


## Debuggen von Diensten

Ausgabe (Logfile) eines Dienstes anzeigen

	sudo journalctl unit=searchwing-payloads-camera.service
	
Ausgabe des gesamten Bootlog

	sudo journalctl
	
## Partitionierung der SD-Karte

Auf der SD-Karte gibt es folgende Partitionen:

* Boot --> `/boot/`
* Root --> `/`
* daten --> `/data/bilder`

Die Partition `/data` ist einzig und allein zum abspeichern der aufgenommen Bilder. Durch diese Aufteilung wird ein volllaufen des root Dateisytems, durch das abspeichern von Bildern, verhindert.

## Client-Anwendung zum Herunterladen der Bilder

Das Skript `download_images.py` ist eine kleines Qt5 Dialogfenster das über rsync die aufgenommen Bilder syncronisiert.


Zum ausführen muss Python3, PyQt5 und rsync vorhanden sein!
