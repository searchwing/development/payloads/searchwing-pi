#!/usr/bin/env python3

import os
import toml
import sys
from src.common import *
from src.searchwing_selftest.SelfTest import SelfTest
from src.searchwing_selftest.SelfTestMavlinkSender import SelfTestMavlinkSender
from src.searchwing_selftest.modules.SelfTestModuleDiskSpace import SelfTestModuleDiskSpace
from src.searchwing_selftest.modules.SelfTestModuleCameraImages import SelfTestModuleCameraImages
from src.searchwing_selftest.modules.SelfTestModuleCheckService import SelfTestModuleCheckService
from src.searchwing_selftest.modules.SelfTestModuleValueZmqReceiver import SelfTestModuleValueZmqReceiver
from src.configParser import ConfigParser
from src.zmqDrivers.zmqConnection import zmqReceiver


if __name__ == "__main__":
    print("############################################################")
    print("Started preflight selftest...")

    configParser = ConfigParser()
    config = configParser.getConfig()

    droneState_Receiver = zmqReceiver("ipc:///dev/shm/droneState")
    droneState_Receiver.set_receive_timeout(2000)
    droneStateMsg = droneState_Receiver.receive_msg()

    if droneStateMsg is None:
        print("No heartbeat from drone received - exit!")
        sys.exit(0)

    if droneStateMsg["data"]["ISFLYING"]:
        print("Drone is probably flying. Abort Preflight payload selftest.")
        sys.exit(0)

    neededDiskSpace = (
        0.005 *                           # GB/frame  (image size)
        1/config['CAM_TRIGGER_PERIODE'] * # frame/sec (image capture frequency)
        60*60 *                           # sec (flightduration)
        1                                 # cameracount
    )

    selftestModules = SelfTest([
        SelfTestModuleCheckService('searchwing-payloads-camera@camr'),
        SelfTestModuleCheckService('searchwing-payloads-camera@caml'),
        SelfTestModuleCheckService('searchwing-payloads-temp-humid'),
        SelfTestModuleCheckService('searchwing-driver-cputemp'),
        SelfTestModuleCheckService('searchwing-driver-dht22'),
        SelfTestModuleCheckService('searchwing-driver-dronestate'),
        # SelfTestModuleCheckService('searchwing-simple-webserver'),
        SelfTestModuleCheckService('mavlink-router'),
        SelfTestModuleDiskSpace("/data", neededDiskSpace),
        SelfTestModuleCameraImages("Camera-r", os.path.join(
            config['CAM_IMG_STORAGE_DIR'], 'preflight_cam-r.jpg'), config['CAM_TRIGGER_PERIODE_PREFLIGHT']+1),
        SelfTestModuleCameraImages("Camera-l", os.path.join(
            config['CAM_IMG_STORAGE_DIR'], 'preflight_cam-l.jpg'), config['CAM_TRIGGER_PERIODE_PREFLIGHT']+1),
        SelfTestModuleValueZmqReceiver(
            "cpuTemp", "ipc:///dev/shm/cputemp", "<", 100.0, 140.0),
        SelfTestModuleValueZmqReceiver(
            "dhtTemp", "ipc:///dev/shm/dht22_temp", "<", 90.0, 130.0),
        SelfTestModuleValueZmqReceiver(
            "dhtHum", "ipc:///dev/shm/dht22_hum", "<", 85.0, 95.0)
    ])

    selftestModules.run_all()
    selftestModules.print_result()

    resultSender = SelfTestMavlinkSender(config['MAVLINK_STATUSTEXT_ENDPOINT'], source_system=1,
                                         source_component=10, buzzer_target_system=1, buzzer_target_component=1)
    resultSender.send_result(selftestModules.get_results())
