
apt-get install --yes vsftpd python3-serial python3-picamera python3-pip python3-numpy python3-dev pypy libxml2-dev libxslt1-dev python3-lxml
pip3 install -r requirements.txt
pip3 install pymavlink
cp root/etc/systemd/system/* /etc/systemd/system

mkdir -p /data/bilder
mkdir -p /data/logs

systemctl daemon-reload

systemctl enable searchwing-payloads-camera@caml.service
systemctl enable searchwing-payloads-camera@camr.service
systemctl enable searchwing-payloads-temp-humid.service
systemctl enable searchwing-preflight-selftest.service
systemctl enable searchwing-preflight-selftest.timer
systemctl enable searchwing-simple-webserver.service
systemctl enable searchwing-driver-dht22.service
systemctl enable searchwing-driver-dronestate.service
systemctl enable searchwing-driver-cputemp.service

systemctl start searchwing-payloads-camera@caml.service
systemctl start searchwing-payloads-camera@camr.service
systemctl start searchwing-payloads-temp-humid.service
systemctl start searchwing-preflight-selftest.service
systemctl start searchwing-preflight-selftest.timer
systemctl start searchwing-simple-webserver.service
systemctl start searchwing-driver-dht22.service
systemctl start searchwing-driver-dronestate.service
systemctl start searchwing-driver-cputemp.service
