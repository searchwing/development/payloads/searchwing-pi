#!/bin/bash
# Make sure only root can run our script
if [[ $EUID -ne 0 ]]; then
	   echo "This script must be run as root" 1>&2
	      exit 1
      fi

if [ $# -eq 0 ]
  then
    echo "QGroundstation IP must be given as first argument"
    exit 1
fi

echo "streaming video to $1"
sudo gst-launch-1.0 v4l2src device=/dev/video0 ! video/x-h264,width=1280,height=720,framerate=24/1 ! h264parse ! rtph264pay ! udpsink host=$1 port=5600
