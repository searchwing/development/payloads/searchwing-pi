apt-get install -y git python-future python3-future libtool autoconf

git clone https://github.com/mavlink-router/mavlink-router/
cd mavlink-router
git submodule update --init --recursive
./autogen.sh && ./configure CFLAGS='-g -O2' --sysconfdir=/etc --localstatedir=/var --libdir=/usr/lib --prefix=/usr
make install

cd ..
mkdir -p /etc/mavlink-router/config.d/
cp root/etc/mavlink-router/config.d/main.conf /etc/mavlink-router/config.d/main.conf

systemctl enable mavlink-router.service
systemctl start mavlink-router.service