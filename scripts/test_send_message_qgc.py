#!/usr/bin/env python3

import time
from datetime import datetime

from pymavlink import mavutil
from pymavlink.dialects.v10 import common as mavlink
from pymavlink.dialects.v10 import ardupilotmega as mavlink1
from pymavlink.dialects.v20 import ardupilotmega as mavlink2

master = mavutil.mavlink_connection(
    'udp:127.0.0.1:14551', source_system=1, source_component=10)
master.wait_heartbeat()

print(master)
print(master.mav)

while True:
    stamp = datetime.now()
    string = stamp.strftime("%Y-%m-%dT%H-%M-%S.%f")
    print("send " + string)
    print(master.mav.statustext_send(
        mavutil.mavlink.MAV_SEVERITY_CRITICAL, str.encode(string)))

    tune = b'ABABAB'
    tune_msg = mavlink2.MAVLink_play_tune_message(200, 0, tune, b'')
    master.mav.send(tune_msg)

    time.sleep(3)
