import itertools
import os

ev = [-10, 0, 10]
ex = ['auto', 'sports', 'beach']

jobs = []
# print(list(itertools.permutations([ev,ex])))
for i, r in enumerate(itertools.product(ev, ex)):
    print(r[0])
    print(r[1])
    job = "raspistill -q 92 -t 1000 -p 0,0,1000,1000 -fli 50hz -ev {} -ex {} -o {}".format(
        r[0], r[1], str(i)+"_img_ev"+str(r[0])+"_ex"+r[1]+".jpg")
    print(job)
    os.system(job)
