import socket
import time
import picamera

print("run first: vlc udp://@:5600 --demux h264")
input()
# Connect a client socket to my_server:5600 (change my_server to the
# hostname of your server)
client_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
client_socket.connect(('192.168.188.3', 5600))

# Make a file-like object out of the connection
connection = client_socket.makefile('wb')
try:
    with picamera.PiCamera() as camera:
        camera.resolution = (1280, 720)
        camera.framerate = 24
        # Start a preview and let the camera warm up for 2 seconds
        # camera.start_preview()
        # time.sleep(2)
        # Start recording, sending the output to the connection for 60
        # seconds, then stop
        camera.start_recording(connection, format='h264',
                               bitrate=5000000, quality=25)
        camera.wait_recording(60)
        camera.stop_recording()
finally:
    connection.close()
    client_socket.close()
